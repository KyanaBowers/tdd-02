function getHeaderName(headerRow) {
  const headerRowTokens = headerRow.split("|");
  const headerNameArray = headerRowTokens.filter((token) => token !== "");
  const headerName = headerNameArray[0].trim();

  return headerName;
}

function getRowValue(row) {
  const rowTokens = row.split("|");
  const rowNameArray = rowTokens.filter((token) => token !== "");
  const rowName = rowNameArray[0].trim();

  return rowName;
}

function parse(table) {
  const headerAndRows = table.split("\n");
  if (table.length === 0) {
    return {
      header: [],
      rows: [],
    };
  } else if (headerAndRows.length === 1) {
    const headerName = getHeaderName(table);

    return { header: [headerName], rows: [] };
  } else {
    const headerName = getHeaderName(table);
    const rowsValue = getRowValue(headerAndRows[1]);

    const rowObject = { [headerName]: rowsValue };
    return { header: [headerName], rows: [rowObject] };
  }
}

module.exports = parse;
